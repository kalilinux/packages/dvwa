#!/bin/sh

set -e

dvwa-start
systemctl status dvwa

if ! systemctl is-active -q dvwa; then
    echo "The service dvwa fails to start"
    exit 1
fi

if ! curl 127.0.0.1:42001/login.php | grep "Username"; then
    echo "FAILURE: Web UI test failed"
    exit 1
fi

dvwa-stop

if systemctl is-active -q dvwa; then
    echo "The service dvwa fails to stop"
    exit 1
fi

