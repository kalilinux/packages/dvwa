dvwa (2.4-0kali1) kali-dev; urgency=medium

  * New upstream version 2.4
  * Update to PHP 8.4

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 10 Jan 2025 15:12:00 +0100

dvwa (2.2.2-0kali2) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Update standards version to 4.6.2, no changes needed.

  [ Sophie Brun ]
  * Fix database creation (https://bugs.kali.org/view.php?id=8884)
  * Bump Standards-Version to 4.7.0

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 10 Sep 2024 14:45:36 +0200

dvwa (2.2.2-0kali1) kali-dev; urgency=medium

  * New upstream version 2.2.2
  * Update debian/* for new release
  * Bump Standards-Version to 4.6.2 (no changes)
  * Update debian/rules
  * Add autopkgtest

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 24 Mar 2023 09:20:13 +0100

dvwa (2.0.1+git20220104-0kali5) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

  [ Sophie Brun ]
  * Update to PHP 8.2
  * Fix typo in postinst
  * Add lintian-overrides

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 23 Jan 2023 17:11:46 +0100

dvwa (2.0.1+git20220104-0kali4) kali-dev; urgency=medium

  * Improve nginx config

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 14 Jun 2022 14:19:14 +0200

dvwa (2.0.1+git20220104-0kali3) kali-dev; urgency=medium

  * debian/control: use versioned php packages to force the update / have the
    same version of all PHP components
  * Use /run in dvwa.service instead of /var/run

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 09 Mar 2022 18:31:56 +0100

dvwa (2.0.1+git20220104-0kali2) kali-experimental; urgency=medium

  * Upgrade to php 8.1
  * Add a README.Debian

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 03 Feb 2022 11:32:59 +0100

dvwa (2.0.1+git20220104-0kali1) kali-experimental; urgency=medium

  * Initial release

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 04 Jan 2022 13:43:18 +0100
